### Trajectory data ###

* `data` contains trajectory datasets on Atlanta, San Jose and King-NewYork road networks

### Tracemob application ###

* Extract source code from `src` folder
* Run the application from the root folder:

```
#!java
java -jar tracemob.jar -DconfigFile=configs/jnlp-demo.xml -DmapInfo=configs/maps/sj-map.txt -J-DtrajectoryData=data/sj2k.txt -DouputEmbeddings=output/trajmap/embs.csv -DoutputClusteringResult=output/clusters/clusters.txt
```